# Teste para a vaga de Frontend - RZ2 

Utilizando Angular, implemente uma tela de login, com os campos de usuário, senha e empresa. Ao preencher os campos e submeter o formulário, a aplicação deve redirecionar o usuário para uma tela de início. Nesta tela devem ser apresentados os dados de login e ter um link ou botão para o usuário fazer logout. Ao efetuar logout, o usuário deve ser redirecionado para a tela de login.

## Requisitos mínimos

* O formulário deve ser validado, não permitindo campos em branco.
* O campo empresa deve ser um _select_, cujas opções são definidas no Typescript, não no html.
* A aplicação deve indicar ao usuário o(s) campo(s) com erro.
* O usuário não pode ser capaz de acessar a tela inicial sem estar logado.
* A aplicação precisa ser responsiva.
* Devem ser utilizados Angular (2 ou superior) e Typescript.
* Preferencialmente, utilizar a abordagem de _Reactive Forms_ ao invés de _Template-driven_.

## Instruções

* Faça um fork deste repositório.
* Desenvolva o código, realizando commits descritivos.
* Ao finalizar, envie um email para __*rafael.abreu@rz2.com.br*__, com o link do repositório.

## Avaliação

A aplicação deve cumprir com os requisitos mínimos estipulados. Além disso, serão avaliados os seguintes pontos:

* Organização e clareza do código
* Conteúdo das mensagens dos _commits_
* Domínio do Angular e do Typescript
* Domínio do CSS e fidelidade ao layout

## Diferenciais

* Uso de SASS
* Utilização de _api mock_ (há exemplos na documentação do Angular)
* Aplicação de testes unitários

## Dúvidas

Em caso de dúvidas, uma _issue_ deve ser criada.