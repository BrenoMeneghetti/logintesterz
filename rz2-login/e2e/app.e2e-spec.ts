import { Rz2LoginPage } from './app.po';

describe('rz2-login App', () => {
  let page: Rz2LoginPage;

  beforeEach(() => {
    page = new Rz2LoginPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
