import { composeAsyncValidators } from '@angular/forms/src/directives/shared';

import { Company } from './company';

export class User {
    userName: string;
    password: string;
    company: Company;
    constructor(userName: string, password: string, company: Company){
      this.userName = userName;
      this.password = password;
      this.company = company;

    }
}
