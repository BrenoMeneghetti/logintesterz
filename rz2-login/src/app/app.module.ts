import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { AdminComponent } from './pages/admin/admin.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

import { AppRoutingModule } from './app-routing.module';

import { fakeBackendProvider } from './mockups/fake-backend';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';


import {AuthGuard} from './guards/auth.guard';
import { CompanyService } from "app/services/company.service";
import {AuthenticationService} from './services/authentication.service';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        AdminComponent,
        NavBarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpModule
    ],
  providers: [
    AuthGuard,
        AuthenticationService,
        CompanyService,
        // providers used to create fake backend
        fakeBackendProvider,
        MockBackend,
        BaseRequestOptions
  ],
    bootstrap: [AppComponent]
})
export class AppModule { }
