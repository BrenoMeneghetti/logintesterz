import { Company } from '../models/company';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CompanyService {

    constructor(private http: Http) {
    }

    getCompanies(): Observable<Company[]> {
        // get users from api
        return this.http.get('/api/companies')
            .map((response: Response) => response.json());
    }

}
