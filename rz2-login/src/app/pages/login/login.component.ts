import { Company } from '../../models/company';

import { Component, NgModule, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CompanyService } from 'app/services/company.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    user: FormGroup;
    errorMessage: string;
    companies: Company[] = [];

    constructor(private zone:NgZone,
      private companyService: CompanyService,
      private authenticationService: AuthenticationService,
      private router: Router) {
    }

    onSubmit() {
        this.authenticationService.login(this.user.value.userName, this.user.value.password, this.user.value.company)
            .subscribe(result => {
                if (result === true) {
                    // login successful
                    this.router.navigate(['/admin']);
                } else {
                    // login failed
                    this.errorMessage = 'Please check your informations and try again';
                    // this.loading = false;
                }
            });
    }

    ngOnInit() {
        this.user = new FormGroup({
            userName: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            company: new FormControl('', Validators.required)
        })
        this.companyService.getCompanies()
            .subscribe(companies => {
              this.zone.run(() => { // <== added
                this.companies = companies;
                 });
            });
        if (localStorage.getItem('currentUser')) {
            this.router.navigate(['/admin']);
        }
    }

}
