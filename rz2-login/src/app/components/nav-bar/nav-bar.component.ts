import { Component, OnInit, NgZone } from '@angular/core';
import { AuthenticationService } from 'app/services/authentication.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
	logged: boolean = false;

    constructor( 
		private authenticationService: AuthenticationService,
		private zone:NgZone,
		private router: Router) { }

    ngOnInit() {
      if (localStorage.getItem('currentUser')) {
            this.logged = true;
        }
		let subscription = this.authenticationService.getLoginStatusObservable()
			.subscribe(
				value => {
					this.zone.run(() => { // <== added
						console.log("value",value);
						this.logged = value;
					});
				}
			
			);
    }

	logout(){
		console.log('LOG OUT!');
		this.authenticationService.logout();
		this.router.navigate(['/']);
		
	}

}
